:sequential_nav: next

..  _how-to-orbital-visualization:

Visualizing orbital shape
=========================


Using the ioncat/ionplot tools to process .ion files
----------------------------------------------------

You can look at the shape of the orbitals by plotting the
contents of the .ion files produced by Siesta. These files are not
easily readable, but the 'ioncat' program can extract the relevant 
pieces of information, and the "ionplot" script can drive 'ioncat' to
plot the desired graphs. For example::

        ionplot -o 1 O

will plot the orbital with number "1" in the O.ion file.::

        ioncat -i O

will print the numbers of the representative orbitals of each nlz
shell (i.e., disregarding the 'm' quantum number, which does not
affect the radial part).::

        ioncat -o 1 O 

will output the data for the first orbital in O.ion.

Type "ioncat -h" to see the full set of options for the program.

The script "ionplot" accepts some of the options (such as -o)
and drives also gnuplot to plot the information.

By combining the two one could plot all the orbitals::

 for i in $(ioncat -i O); do ionplot -o $i; done

Each orbital will appear in a different window.

Using sisl to process .ion.nc files
-----------------------------------

When Siesta is compiled with netcdf support, information about basis
orbitals, KB projectors, etc, is also written to .ion.nc files. These
netcdf files can be read by `sisl <http://zerothi.github.io/sisl>`_.

In particular, plots that include orbital information as one of the
panels can be obtained, for example, by executing the ``sdata`` tool
of the sisl distribution::

   sdata Al.ion.nc --plot

.. note::
   You might need to install sisl and/or activate a specific
   python environment on your machine to access its functionality.



