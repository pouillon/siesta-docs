.. _reference_plstm:


plstm: A program to slice LDOS files for STM images
***************************************************

Given an LDOS file, the program ``plstm`` can extract a 2D section in
*constant-height* or *constant-current* mode, optionally projected on
spin components. Non-collinear and spin-orbit modes are supported.

.. code-block::
   
   -------------------
   Usage: plstm [options] LDOSfile
   
   
   OPTIONS: 
   
   -h             Print this help
   -d             Print debugging info
   
   -i current     Constant-current calculation
                  with 'current' in e/bohr**3 
   -z height      Constant-height calculation
                  with 'height' in bohr      
   
   -s {q,x,y,z,s} Spin code (default 'q' for total 'charge')
                  (x|y|z) select cartesian components of spin 
                  s selects total spin magnitude            
   -v 'ux uy uz'  Tip spin direction for selection of spin component
                  The vector (ux,uy,uz) should be normalized
   
   -X NX          Request multiple copies of plot domain along X
   -Y NY          Request multiple copies of plot domain along Y
   
   -o OUTPUT_FILE Set output file name, overriding conventions
   
   -H/-I          Stop after computing ranges of heights and currents
   -------------------

The 2D section is ready to be plotted by gnuplot. Implementations of other
post-processing options are welcome.

   
