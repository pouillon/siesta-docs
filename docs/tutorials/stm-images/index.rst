:sequential_nav: next

..  _tutorial-stm-images:

Simulation of STM images
============================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

    
.. note::
   For some background and a gallery of images, see the `slide
   presentation
   <https://drive.google.com/file/d/1Ev9gDyle8NbzH2SJ6eTVl2-n9lBeozMw/view?usp=sharing>`_
   by Jorge Pilo.

.. note::
   This tutorial has the work-files, but the top-level description of
   each task is not yet ready in text format: it is embedded in the
   above presentation.

This tutorial provides some guided examples for the use of the tools
available to simulate STM and STS images with Siesta. An overview of
the tools in provided :ref:`here<how-to-stm-sts-simulation>`. 

STM image of a benzene molecule
-------------------------------

STM image from direct LDOS calculation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

STM image from wavefunction information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


STM images for a monolayer of Cr with Spin-Orbit coupling
---------------------------------------------------------

Images with magnetic tips
~~~~~~~~~~~~~~~~~~~~~~~~~

   
   
