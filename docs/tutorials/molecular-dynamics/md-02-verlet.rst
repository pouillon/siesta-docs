:sequential_nav: next

..  _tutorial-md-verlet:

Molecular dynamics in the microcanonical ensemble
=================================================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.


In this exercise you will play with different MD options.

The system under study is a cubic supercell of 8 atoms of Si in the
diamond structure. In order to make the runs fast, we use a single- Z
basis set with short orbitals (energy shift 300 meV).

Verlet dynamics

Go to the directory verlet. Edit the si8.fdf file and study it. It is
an input file for a simple (microcanonical) MD simulation with the
Verlet algorithm. The initial positions correpond to the ideal crystal
structure. The inicial velocity is 300 K. The time step is 3 fs, and
the simulation is run for 200 steps.

Run the simulation and analyze the result.
Look at the si8.MDE file, and plot:

- total energy and KS energy versus time. Check the energy
   conservation

- temperature versus time. Why is the average temperature smaller than
  the initial one?

