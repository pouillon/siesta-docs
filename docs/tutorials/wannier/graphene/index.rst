:sequential_nav: next

..  _tutorial-wannier-graphene:

Disentanglement (example: graphene)
============================================

In this tutorial, we will learn how to set the energy windows for band disentanglement. We will take the example of graphene.

* :download:`Exercise-Wannier90-graphene.pdf<work-files/Exercise-Wannier90-graphene.pdf>`



You can download the needed file for this tutorial.

* :download:`C.psf<work-files/C.psf>`
* :download:`graphene.fdf<work-files/graphene.fdf>`
* :download:`graphene.win<work-files/graphene.win>`

