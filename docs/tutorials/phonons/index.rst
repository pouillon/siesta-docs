:sequential_nav: next

..  _tutorial-advanced-phonons:

Advanced topics in phonons
==========================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

.. note::
   For background, see the
   excellent `slide presentation <https://drive.google.com/file/d/1SRhSOWwFVPmS3vr4REJSvqgXf2Tic8fl/view?usp=sharing>`_
   by Andrei Postnikov. 

.. note::
   Topics: Phonon (projected)DOS, Born-effective charges and infrared
   activity. LO-TO splitting (maybe). Post-processing with Andrei's
   lattice-dynamics tools.

.. note::
   Exercises: Use the newer examples by Andrei on a BN
   monolayer and a Au chain. For LO-TO and infrared, we need
   examples. Actually, for LO-TO splitting we need to fish out Thomas
   Archer's vibra code.

   
   
