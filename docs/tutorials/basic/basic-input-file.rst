:sequential_nav: next

..  _tutorial-basic-input-file:

The Siesta FDF input file
=========================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

The main input for Siesta is a text file in FDF format (ref to FDF
docs), which contains a description of the system to be simulated, a
(maybe implicit) script of actions to be taken, and a specification of
the quality parameters of the simulation.




