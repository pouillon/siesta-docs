:sequential_nav: next

..  _tutorial-basic-pseudopotentials:

Pseudopotentials
================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will discuss some basic concepts of
the use of pseudopotentials in Siesta:

* Types of pseudopotential files and where to find them
* Core/valence configuration of the pseudopotential and its influence
  on basis-set generation.
* The Kleinman-Bylander transformation and its options. Ghosts.
* Core corrections and their influence on the mesh-cutoff needed.
* ...
  
For more information on actually generating and testing a pseudopotential, see
:ref:`the 'advanced' tutorial<tutorial-pseudopotentials>`.
   
