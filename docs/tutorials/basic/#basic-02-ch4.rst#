:sequential_nav: next

..  _tutorial-basic-ch4:

A first encounter with Siesta
========================================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will focus on the basic input parameters and
outputs of the SIESTA program.  We will study two simple molecules
(CH4 and CH3)
 
..note:: (Enter the directory 'CH4')

You will find an input file for the SIESTA program named ch4.fdf along
with the files C.psf and H.psf containing the information about the
pseudopotentials.  The ch4.fdf file sets the value of several
parameters which specify both the system we want to study and the
accuracy of the calculation. Of course the parameters specifying the
system are mandatory (number of atoms, atomic numbers, label
associated with each atomic specie, etc..). All other parameters have
some default values and, in principle, it is not necessary to
explicitly include them in the input file. However, it is important to
note that the default values do not always guarantee a converged
calculation.
 
In the file ch4.fdf you can find first the inputs that specify the
system. Pay special attention to the block ChemicalSpeciesLabel. In
this block you assign an index and a label to each atomic species. The
label will allow to recognize the files containing the information
about the pseudopotential and the basis set (when provided).
 
Check the input of the coordinates (they are just some guess
coordinates, not the optimized equilibrium ones).
 
The file ch4.fdf contains the most important parameters to take into
account to perform a molecular calculation. Namely:



