:sequential_nav: next

..  _tutorial-basic-kpoint-convergence:

Sampling of the BZ with k-points
================================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will look more closely at the k-point sampling
of the BZ for crystals.


.. note::
   Objectives: specification of sampling, including displacement of
   the kgrid. Kgrid-cutoff concept.
   Test systems: do we include here graphene? Specific gotchas for
   some systems related to symmetry or sampling?
   Distinction between scf sampling and other samplings (bands,
   (p)DOS, etc).  Convergence monitoring.
   

   
  
  
   

   
