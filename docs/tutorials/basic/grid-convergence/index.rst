:sequential_nav: next

..  _tutorial-basic-grid-convergence:

The real-space grid
===================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will look more closely at the real-space grid
and how to monitor its adequacy for a given system.

.. warning::
   Rest of file under construction using
   material from the old `GridConvergence` tutorials.

Figures:  Basic sketch of grid and orbitals footprints. Some model
results.


* Directory CH4:

  focus on convergence of properties with mesh-cutoff

* Directory MgO:

  focus on the egg-box effect and two methods to
  alleviate it: increase mesh-cutoff and use of 'grid-cell-sampling'.

  Optionally: suggest using Lua to automate the displacements.
  
  
   

   
